package com.unigames.repository;

import com.unigames.model.Buy;
import com.unigames.model.User;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface BuyRepository extends CrudRepository<Buy, Long> {
   
    
}