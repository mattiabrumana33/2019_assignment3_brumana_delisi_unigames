package com.unigames.repository;

import com.unigames.model.Platform;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface PlatformRepository extends CrudRepository<Platform, Long> {
    public Platform findById(Long id);
}