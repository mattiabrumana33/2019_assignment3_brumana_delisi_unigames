package com.unigames.repository;

import com.unigames.model.Rent;
import com.unigames.model.User;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface RentRepository extends CrudRepository<Rent, Long> {
   
    
}