package com.unigames.repository;

import com.unigames.model.DeluxeUser;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface DeluxeUserRepository extends CrudRepository<DeluxeUser, Long> {
   public DeluxeUser findById(Long id); 
}