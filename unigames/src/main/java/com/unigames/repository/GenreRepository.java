package com.unigames.repository;

import com.unigames.model.Genre;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface GenreRepository extends CrudRepository<Genre, Long> {
    public Genre findById(Long id);
}