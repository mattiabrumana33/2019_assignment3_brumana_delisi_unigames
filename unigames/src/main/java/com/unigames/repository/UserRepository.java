package com.unigames.repository;

import com.unigames.model.User;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
   public User findById(Long id);
   public User findByEmail(String email);
}