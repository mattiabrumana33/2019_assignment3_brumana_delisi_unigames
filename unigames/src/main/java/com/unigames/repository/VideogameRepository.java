package com.unigames.repository;

import com.unigames.model.Videogame;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface VideogameRepository extends CrudRepository<Videogame, Long> {
    public Videogame findById(Long id);
    public Videogame findByTitle(String title);
}