package com.unigames.model;

import java.util.*;

import javax.persistence.*;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long id;
    public String name;
    public String surname;
    public String email;
    public String fiscal_code;
    public String password;
    public String type;

    @OneToMany(mappedBy = "user")
    public List<Buy> videogamesBought;

    public User() {
        super();
    }

    public User(String name, String surname, String cf,
            String email, String password) {
        super();
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.fiscal_code = cf;
        this.password = password;
        this.videogamesBought = new ArrayList<Buy>();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

     public String getFiscalCode() {
        return fiscal_code;
    }

    public void setFiscalCode(String fiscal_code) {
        this.fiscal_code = fiscal_code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Buy> getVideogamesBought() {
        return videogamesBought;
    }

    public void setVideogamesBought(List<Buy> vgBought) {
        this.videogamesBought = vgBought;
    } 
}