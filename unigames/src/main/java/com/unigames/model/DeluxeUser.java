package com.unigames.model;

import java.util.*;

import javax.persistence.*;

@Entity
public class DeluxeUser extends User {
    @OneToMany(mappedBy = "deluxeUser")
    private List<Rent> videogamesRented;

    public DeluxeUser() {
        super();
    }

    public DeluxeUser(String name, String surname, String email,
            String cf, String password) {
        super();
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.fiscal_code = cf;
        this.password = password;
        this.type = "deluxe";
        this.videogamesBought = new ArrayList<Buy>();
        this.videogamesRented = new ArrayList<Rent>();
    }

    public List<Rent> getVideogamesRented() {
        return videogamesRented;
    }

    public void setVideogamesRented(List<Rent> vgRented) {
        this.videogamesRented = vgRented;
    }
}