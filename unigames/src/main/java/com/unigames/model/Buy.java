package com.unigames.model;

import java.util.*;

import javax.persistence.*;

@Entity
public class Buy {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Videogame videogame;

    @ManyToOne
    private User user;

    private String date;
    private String n_copies;
    private String total;

    public Buy() {
        super();
    }

    public Buy(Videogame vg, User us, String date, String n_copies, String total) {
        super();
        this.videogame = vg;
        this.user = us;
        this.date = date;
        this.n_copies = n_copies;
        this.total = total;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Videogame getVideogame() {
        return videogame;
    }

    public void setVideogame(Videogame vg) {
        this.videogame = vg;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNcopies() {
        return n_copies;
    }

    public void setNcopies(String n_copies) {
        this.n_copies = n_copies;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}