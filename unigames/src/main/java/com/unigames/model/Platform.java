package com.unigames.model;

import java.util.*;

import javax.persistence.*;

@Entity
public class Platform {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Videogame> videogames;

    public Platform() {
        super();
    }

    public Platform(String name) {
        super();
        this.name = name;
        this.videogames = new ArrayList<Videogame>(); 
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Videogame> getVideogames() {
        return videogames;
    }

    public void setVideogames(List<Videogame> videogames) {
        this.videogames = videogames;
    }

}