package com.unigames.model;

import java.util.*;

import javax.persistence.*;

@Entity
public class Videogame {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String release_year;
    private String developer;
    private String n_copies;
    private String price;
    private String online;
    private String multiplayer;

    @ManyToMany
    private List<Videogame> prequel;

    @ManyToMany(mappedBy = "videogames")
    private List<Platform> platformsAvailables;

    @ManyToMany(mappedBy = "videogames")
    private List<Genre> genresVideogame;

    @OneToMany(mappedBy = "videogame")
    private List<Buy> usersBought;

    @OneToMany(mappedBy = "videogame")
    private List<Rent> usersRented;

    public Videogame() {
        super();
    }

    public Videogame(String title, String release_year, String developer,
            String n_copies, String price, String online, String multiplayer, 
            List<Platform> platformsAvailables, List<Genre> genresVideogame)  {
        super();
        this.title = title;
        this.release_year = release_year;
        this.developer = developer;
        this.n_copies = n_copies;
        this.price = price;
        this.online = online;
        this.multiplayer = multiplayer;
        this.prequel = new ArrayList<Videogame>();
        this.platformsAvailables = platformsAvailables;
        this.genresVideogame = genresVideogame;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseYear() {
        return release_year;
    }

    public void setReleaseYear(String release_year) {
        this.release_year = release_year;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getNcopies() {
        return n_copies;
    }
    
    public void setNcopies(String n_copies) {
        this.n_copies = n_copies;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
    
    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getMultiplayer() {
        return multiplayer;
    }

    public void setMultiplayer(String multiplayer) {
        this.multiplayer = multiplayer;
    }

    public List<Videogame> getPrequel() {
        return prequel;
    }

    public void setPrequel(List<Videogame> preq) {
        this.prequel = preq;
    }

    public List<Platform> getPlatforms() {
        return platformsAvailables;
    }

    public void setPlatforms(List<Platform> platforms) {
        this.platformsAvailables = platforms;
    }

    public List<Genre> getGenres() {
        return genresVideogame;
    }

    public void setGenres(List<Genre> genres) {
        this.genresVideogame = genres;
    }

    public List<Buy> getUsersBought() {
        return usersBought;
    }

    public void setUsersBought(List<Buy> usersBought) {
        this.usersBought = usersBought;
    }

    public List<Rent> getUsersRented() {
        return usersRented;
    }

    public void setUsersRented(List<Rent> usersRented) {
        this.usersRented = usersRented;
    }
}