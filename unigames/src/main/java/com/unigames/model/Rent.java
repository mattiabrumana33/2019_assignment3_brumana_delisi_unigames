package com.unigames.model;

import java.util.*;

import javax.persistence.*;

@Entity
public class Rent {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Videogame videogame;

    @ManyToOne
    private DeluxeUser deluxeUser;

    private String start_date;
    private String end_date;
    private String n_copies;
    private String total;

    public Rent() {
        super();
    }

    public Rent(Videogame vg, DeluxeUser us, String start_date, String end_date, String n_copies, String total) {
        super();
        this.videogame = vg;
        this.deluxeUser = us;
        this.start_date = start_date;
        this.end_date = end_date;
        this.n_copies = n_copies;
        this.total = total;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Videogame getVideogame() {
        return videogame;
    }

    public void setVideogame(Videogame vg) {
        this.videogame = vg;
    }

    public DeluxeUser getUser() {
        return deluxeUser;
    }

    public void setUser(DeluxeUser user) {
        this.deluxeUser = user;
    }

    public String getStartDate() {
        return start_date;
    }

    public void setStartDate(String date) {
        this.start_date = date;
    }

    public String getEndDate() {
        return end_date;
    }

    public void setEndDate(String date) {
        this.end_date = date;
    }

    public String getNcopies() {
        return n_copies;
    }

    public void setNcopies(String n_copies) {
        this.n_copies = n_copies;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}