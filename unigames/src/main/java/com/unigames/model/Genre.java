package com.unigames.model;

import java.util.*;

import javax.persistence.*;

@Entity
public class Genre {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String type;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Videogame> videogames;

    public Genre() {
        super();
    }

    public Genre(String type) {
        super();
        this.type = type;
        this.videogames = new ArrayList<Videogame>();
    
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Videogame> getVideogames() {
        return videogames;
    }

    public void setVideogames(List<Videogame> videogames) {
        this.videogames = videogames;
    }
}