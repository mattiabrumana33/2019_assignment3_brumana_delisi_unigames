package com.unigames.model;

import java.util.*;

import javax.persistence.*;

@Entity
public class BasicUser extends User {
    public BasicUser() {
        super();
    }

    public BasicUser(String name, String surname, 
            String email, String cf, String password) {
        super();
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.fiscal_code = cf;
        this.password = password;
        this.type = "basic";
        this.videogamesBought = new ArrayList<Buy>();
    }
}