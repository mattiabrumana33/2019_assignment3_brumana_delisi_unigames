package com.unigames.controller;

import com.unigames.model.*;
import com.unigames.repository.*;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    VideogameRepository videogameRepository;

    @RequestMapping(value="/userLogin", method=RequestMethod.POST)
    public String userLogin(@RequestParam String em, @RequestParam String psw, Model model) {
       try
       { User user = userRepository.findByEmail(em);

        if (em.equals("admin") && psw.equals("admin")) {
            return "redirect:/indexAdmin";
        }

        if (em.equals(user.getEmail()) && psw.equals(user.getPassword())) {
            Long id = user.getId();
            return "redirect:/" + id + "/videogames/";
        }
        
        return "redirect:/index";
       }catch(Exception e)
		{
			model.addAttribute("type",4);
			model.addAttribute("errorString","Email or Password are incorrect... Please try again");
			return "errors";

		}	
    }

    @RequestMapping(value="/userRegister", method=RequestMethod.POST)
    public String userRegister(@RequestParam String name, @RequestParam String surname,@RequestParam String em, 
                            @RequestParam String cf, @RequestParam String psw, 
                            @RequestParam String basic, Model model) {
        if (basic != "off"){
            User user = new BasicUser(name,surname,em,cf,psw);
            
            userRepository.save(user);  

            model.addAttribute("user", user); 
            Long id = user.getId(); 

            return "redirect:/" + id + "/videogames/";
        }
        else {
            User user = new DeluxeUser(name,surname,em,cf,psw);
            
            userRepository.save(user);

            model.addAttribute("user", user);
            Long id = user.getId(); 
        
            return "redirect:/" + id + "/videogames/";
        }
    }

    @RequestMapping(value="/account/{id}", method=RequestMethod.GET)
	public String accountList(@PathVariable Long id, Model model) {
		User u = userRepository.findOne(id);
		model.addAttribute("user",u);
		
        return "account";
	}

	@RequestMapping(value="/accountDeluxe/{id}", method=RequestMethod.GET)
	public String accountDeluxeList(@PathVariable Long id, Model model) {
		User u = userRepository.findOne(id);
		model.addAttribute("user",u);
		
        return "accountDeluxe";
	}
}