package com.unigames.controller;

import com.unigames.model.*;
import com.unigames.repository.*;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class GenreController {

	@Autowired
	GenreRepository genreRepository;

	@RequestMapping("/editGenre/{id}")
	public String genre(@PathVariable Long id, Model model) {
        model.addAttribute("genre", genreRepository.findOne(id));
        return "editGenre";
	}

  	@RequestMapping(value="/genresAdmin", method=RequestMethod.GET)
	public String genreList(Model model) {
        model.addAttribute("genres", genreRepository.findAll());
        return "genresAdmin";
	}

	@RequestMapping(value="/newGenre")
	public String newGenre() {
		return "newGenre";
	}

	@RequestMapping(value="/newGenre", method=RequestMethod.POST)
	public String GenreAdd(@RequestParam String type, Model model) {
        	Genre gn = new Genre();
        	gn.setType(type);
        	genreRepository.save(gn);

        model.addAttribute("genres", gn);
        return "redirect:/genresAdmin/";
	}

	@RequestMapping(value="/editGenre/{id}", method=RequestMethod.POST)
	public String GenreUpdate(@PathVariable Long id, @RequestParam String type, Model model) {
			Genre genre = genreRepository.findOne(id);

			genre.setType(type);

			genreRepository.save(genre);
			
			return "redirect:/genresAdmin/";	
	}

  	@RequestMapping(value="/deleteGenre/{id}", method=RequestMethod.GET)
	public String GenreDelete(@PathVariable Long id) {
		Genre gn = genreRepository.findOne(id);

		List<Videogame> videogames = gn.getVideogames();

		for(Videogame v:videogames) {
			v.getGenres().remove(gn);
		}

		gn.setVideogames(null);
		genreRepository.save(gn);

		genreRepository.delete(id);
       	return "redirect:/genresAdmin/";
  	}
}