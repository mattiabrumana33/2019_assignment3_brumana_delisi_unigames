package com.unigames.controller;

import com.unigames.model.*;
import com.unigames.repository.*;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class PlatformController {

	@Autowired
	PlatformRepository platformRepository;

	@Autowired
	VideogameRepository videogameRepository;

	@RequestMapping("/editPlatform/{id}")
	public String platform(@PathVariable Long id, Model model) {
        model.addAttribute("platform", platformRepository.findOne(id));
        return "editPlatform";
	}

  	@RequestMapping(value="/platformsAdmin", method=RequestMethod.GET)
	public String platformList(Model model) {
        model.addAttribute("platforms", platformRepository.findAll());
        return "platformsAdmin";
	}

	@RequestMapping(value="/newPlatform")
	public String newPlatform() {
		return "newPlatform";
	}

	@RequestMapping(value="/newPlatform", method=RequestMethod.POST)
	public String PlatformAdd(@RequestParam String name, Model model) {
        	Platform pl = new Platform();
        	pl.setName(name);
        	platformRepository.save(pl);

        model.addAttribute("platforms", pl);
        return "redirect:/platformsAdmin/";
	}

	@RequestMapping(value="/editPlatform/{id}", method=RequestMethod.POST)
	public String PlatformUpdate(@PathVariable Long id, @RequestParam String name, Model model) {
			Platform platform = platformRepository.findOne(id);

			platform.setName(name);

			platformRepository.save(platform);
			
			return "redirect:/platformsAdmin/";	
	}

  	@RequestMapping(value="/deletePlatform/{id}", method=RequestMethod.GET)
	public String PlatformDelete(@PathVariable Long id) {
		Platform pl = platformRepository.findOne(id);

		List<Videogame> videogames = pl.getVideogames();

		for(Videogame v:videogames) {
			v.getPlatforms().remove(pl);
		}

		pl.setVideogames(null);
		platformRepository.save(pl);

		platformRepository.delete(id);
       	return "redirect:/platformsAdmin/";
  	}
}
