package com.unigames.controller;

import com.unigames.model.*;
import com.unigames.repository.*;

import java.util.*;
import java.text.*;
import java.time.*;
import java.lang.Math;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class VideogameController {

	@Autowired
	VideogameRepository videogameRepository;

	@Autowired
	PlatformRepository platformRepository;

	@Autowired
	GenreRepository genreRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	DeluxeUserRepository deluxeUserRepository;

	@Autowired
	BuyRepository buyRepository;

	@Autowired
	RentRepository rentRepository;

	@RequestMapping(value="/{id}/videogames", method=RequestMethod.GET)
	public String videogameList(@PathVariable Long id, Model model) {
		model.addAttribute("videogames", videogameRepository.findAll());
		model.addAttribute("user", userRepository.findOne(id));
	
        return "videogames";
	}

	@RequestMapping(value="/{id}/search", method=RequestMethod.GET)
	public String searchVg(@RequestParam String searchString, @RequestParam String category,  @PathVariable Long id, Model model) {
		List<Videogame> allVg = (List<Videogame>) videogameRepository.findAll();
		List<Videogame> selVg = new ArrayList<Videogame>();
		
		if (category.equals("all"))
        {
          
            if (searchString.equals(" ")){
				selVg = allVg;
            }else
            {   
                for (Videogame vg : allVg)
                {
                    List<Platform> vgPlatforms = vg.getPlatforms();
                    List<Genre> vgGenres = vg.getGenres();

                    boolean ok =true;
                    if(vg.getTitle().toLowerCase().contains(searchString.toLowerCase()) && ok )
                    { 
                        selVg.add(vg);
                        ok=false;

                    }
                    else
                    {   

						for (Platform pl : vgPlatforms)
						{ 
							if(pl.getName().toLowerCase().contains(searchString.toLowerCase()) && ok)
							{   
								selVg.add(vg);
								ok= false;
							}
						}
						for (Genre gn : vgGenres)
						{ 
							if(gn.getType().toLowerCase().contains(searchString.toLowerCase()) && ok)
							{
								selVg.add(vg);
								ok = false;
							}
						}

					}
            	}
			}
		}else if (category.equals("title"))
		{
			if (searchString.equals(" ")){
				selVg = allVg;
			}else
			{   
				for (Videogame vg : allVg)
				{
					if(vg.getTitle().toLowerCase().contains(searchString.toLowerCase())){selVg.add(vg);}

				}
			}
		} else if (category.equals("min_price"))
		{
		
	
			float minPrice =  Float.parseFloat(searchString);
			
			for (Videogame vg : allVg)
			{
				Float priceVg = Float.parseFloat(vg.getPrice());

				if( priceVg > minPrice ) {selVg.add(vg);}
			}
		} else if (category.equals("max_price"))
		{
	
			float maxPrice =  Float.parseFloat(searchString);
			for (Videogame vg : allVg)
			{
				if(Float.parseFloat(vg.getPrice()) < maxPrice ) {selVg.add(vg);}
			}
		} else if (category.equals("prequel"))
		{
			for (Videogame vg : allVg)
			{
				if(vg.getTitle().toLowerCase().contains(searchString.toLowerCase()))
				{

					List<Videogame> prequel = vg.getPrequel();

					if(!(prequel.isEmpty()))
					{
						for (Videogame pr : prequel)
						{ 
							selVg.add(pr);
						}
					}
				}
			}
		}else if (category.equals("platforms"))
		{
			 for (Videogame vg : allVg)
			{
				List<Platform> vgPlatforms = vg.getPlatforms();
				for (Platform pl : vgPlatforms)
				{ 
					if(pl.getName().toLowerCase().contains(searchString.toLowerCase()))
					{   
						selVg.add(vg);
					
					}
				}
			}
		}else if (category.equals("genres"))
		{
			 for (Videogame vg : allVg)
			{
				List<Genre> vgGenres = vg.getGenres();
				for (Genre gn : vgGenres)
				{ 
					if(gn.getType().toLowerCase().contains(searchString.toLowerCase()))
					{   
						selVg.add(vg);
					
					}
				}
			}
		}		
		
		model.addAttribute("videogames", selVg);
		model.addAttribute("user", userRepository.findOne(id));

		return "videogames";
	}

	@RequestMapping(value="/videogamesAdmin", method=RequestMethod.GET)
	public String videogamesAdmin(Model model) {
		model.addAttribute("videogamesAdmin", videogameRepository.findAll());
		return "videogamesAdmin";
	}

	@RequestMapping("/editVideogame/{id}")
	public String videogame(@PathVariable Long id, Model model) {
        model.addAttribute("videogame", videogameRepository.findOne(id));
        return "editVideogame";
	}

	@RequestMapping("/newVideogamePlatformGenre/{id}")
	public String newVideogamePlatformGenre(@PathVariable Long id, Model model) {
        model.addAttribute("videogame", videogameRepository.findOne(id));
		model.addAttribute("platform", platformRepository.findAll());
		model.addAttribute("genre", genreRepository.findAll());
	
        return "newVideogamePlatformGenre";
	}

	@RequestMapping("/newPrequel/{id}")
	public String newPrequel(@PathVariable Long id, Model model) {
		 model.addAttribute("videogame", videogameRepository.findOne(id));
		 model.addAttribute("videogames", videogameRepository.findAll());

		 return "newPrequel";
	} 

	@RequestMapping("/editVideogamePlatformGenre/{id}")
	public String editVideogamePlatformGenre(@PathVariable Long id, Model model) {
        model.addAttribute("videogame", videogameRepository.findOne(id));
		model.addAttribute("platform", platformRepository.findAll());
		model.addAttribute("genre", genreRepository.findAll());
	
        return "editVideogamePlatformGenre";
	}	

	@RequestMapping("/editPrequel/{id}")
	public String editVideogamePrequel(@PathVariable Long id, Model model) {
        model.addAttribute("videogame", videogameRepository.findOne(id));
		model.addAttribute("videogames", videogameRepository.findAll());
		
        return "editPrequel";
	}

	@RequestMapping("/{id2}/infoVideogame/{id}")
	public String infoVideogame(@PathVariable Long id, @PathVariable Long id2, Model model) {
        model.addAttribute("videogame", videogameRepository.findOne(id));
		model.addAttribute("user", userRepository.findOne(id2));
        return "infoVideogame";
	}

	@RequestMapping("/infoVideogameAdmin/{id}")
	public String infoVideogameAdmin(@PathVariable Long id, Model model) {
        model.addAttribute("videogame", videogameRepository.findOne(id));
        return "infoVideogameAdmin";
	}

	@RequestMapping(value="/newVideogame")
	public String newVideogame() {
		return "newVideogame";
	}

	@RequestMapping(value="/newVideogame", method=RequestMethod.POST)
	public String VideogameAdd(
        @RequestParam String title, @RequestParam String release_year, @RequestParam String developer,
        @RequestParam String n_copies, @RequestParam String price, @RequestParam String online, 
        @RequestParam String multiplayer, Model model) {
        	Videogame vg = new Videogame();
        	vg.setTitle(title);
        	vg.setReleaseYear(release_year);
        	vg.setDeveloper(developer);
        	vg.setNcopies(n_copies);
        	vg.setPrice(price);
        	vg.setOnline(online);
			vg.setMultiplayer(multiplayer);
        	videogameRepository.save(vg);
			Videogame v = videogameRepository.findByTitle(vg.getTitle());
			Long id = v.getId();
			
			model.addAttribute("videogames", vg);
		
        	return "redirect:/newVideogamePlatformGenre/" + id;
	}

	@RequestMapping(value="/editVideogame/{id}", method=RequestMethod.POST)
	public String VideogameUpdate(
		@PathVariable Long id, @RequestParam String title, @RequestParam String release_year, @RequestParam String developer,
        @RequestParam String n_copies, @RequestParam String price, @RequestParam String online, 
        @RequestParam String multiplayer, Model model) {
			Videogame videogame = videogameRepository.findOne(id);

			videogame.setTitle(title);
			videogame.setReleaseYear(release_year);
			videogame.setDeveloper(developer);
			videogame.setNcopies(n_copies);
			videogame.setPrice(price);
			videogame.setOnline(online);
			videogame.setMultiplayer(multiplayer);

			videogameRepository.save(videogame);
			
			return "redirect:/videogamesAdmin/";	
	}

  	@RequestMapping(value="/deleteVideogame/{id}", method=RequestMethod.GET)
	public String VideogameDelete(@PathVariable Long id, Model model) {
		Videogame vg = videogameRepository.findOne(id);

		List<Platform> platforms = vg.getPlatforms();
		List<Genre> genres = vg.getGenres();

		try{
			for(Platform p:platforms) {
				p.getVideogames().remove(vg);
			}

			for(Genre g:genres) {
				g.getVideogames().remove(vg);
			}

			vg.setPlatforms(null);
			vg.setGenres(null);
			videogameRepository.save(vg);
			
			videogameRepository.delete(id);
			return "redirect:/videogamesAdmin/";
		}catch(Exception e)
		{
			vg.setPlatforms(platforms);
			vg.setGenres(genres);
			videogameRepository.save(vg);
			
			for(Platform p:platforms) {
				p.getVideogames().add(vg);
				platformRepository.save(p);
			}

			for(Genre g:genres) {
				g.getVideogames().add(vg);
				genreRepository.save(g);
			}

			model.addAttribute("errorString","Sorry... but the game can't be eliminated!");
			model.addAttribute("type",3);
			return "errors";

		}	
  	}

	@RequestMapping(value="/deletePl/{id1}/{id2}/{path}", method=RequestMethod.GET)
	public String deleteVideogamePlatform(@PathVariable Long id1, @PathVariable Long id2, @PathVariable int path) {
		Videogame vg = videogameRepository.findOne(id1);
		Platform pl = platformRepository.findOne(id2);

		vg.getPlatforms().remove(pl);
		pl.getVideogames().remove(vg);

		videogameRepository.save(vg);
		platformRepository.save(pl);
		
		if (path ==	1)
		{
			return "redirect:/newVideogamePlatformGenre/{id1}";
		}else
		{
			return "redirect:/editVideogamePlatformGenre/{id1}";
		}
	}


	@RequestMapping(value="/addPl/{id1}/{id2}/{path}", method=RequestMethod.GET)
	public String addVideogamePlatform(@PathVariable Long id1, @PathVariable Long id2, @PathVariable int path) {
		Videogame vg = videogameRepository.findOne(id1);
		Platform pl = platformRepository.findOne(id2);
		
		List<Platform> pl_already_inserted = vg.getPlatforms();

		for(Platform p:pl_already_inserted) {
			if (p.getId() == id2) {
				if (path == 1)
					{
						return "redirect:/newVideogamePlatformGenre/{id1}";
				}else
					{
						return "redirect:/editVideogamePlatformGenre/{id1}";
					}
			}
		}

		vg.getPlatforms().add(pl);
		pl.getVideogames().add(vg);
		
		videogameRepository.save(vg);
		platformRepository.save(pl);

		if (path == 1)
		{
			return "redirect:/newVideogamePlatformGenre/{id1}";
		}else
		{
			return "redirect:/editVideogamePlatformGenre/{id1}";
		}

		
	}

	@RequestMapping(value="/deleteGn/{id1}/{id2}/{path}", method=RequestMethod.GET)
	public String deleteVideogameGenre(@PathVariable Long id1, @PathVariable Long id2, @PathVariable int path ) {
		Videogame vg = videogameRepository.findOne(id1);
		Genre gn = genreRepository.findOne(id2);

		vg.getGenres().remove(gn);
		gn.getVideogames().remove(vg);

		videogameRepository.save(vg);
		genreRepository.save(gn);

		if (path == 1)
		{
			return "redirect:/newVideogamePlatformGenre/{id1}";
		}else
		{
			return "redirect:/editVideogamePlatformGenre/{id1}";
		}

	}

	@RequestMapping(value="/addGn/{id1}/{id2}/{path}", method=RequestMethod.GET)
	public String addVideogameGenre(@PathVariable Long id1, @PathVariable Long id2, @PathVariable int path) {
		Videogame vg = videogameRepository.findOne(id1);
		Genre gn = genreRepository.findOne(id2);

		List<Genre> gn_already_inserted = vg.getGenres();

		for(Genre g:gn_already_inserted) {
			if(g.getId() == id2) {

				if (path == 1)
					{
						return "redirect:/newVideogamePlatformGenre/{id1}";
				}else
					{
						return "redirect:/editVideogamePlatformGenre/{id1}";
					}
			}
		}

		vg.getGenres().add(gn);
		gn.getVideogames().add(vg);

		videogameRepository.save(vg);
		genreRepository.save(gn);

		if (path == 1)
		{
			return "redirect:/newVideogamePlatformGenre/{id1}";
		}else
		{
			return "redirect:/editVideogamePlatformGenre/{id1}";
		}

	}

	@RequestMapping(value="/deletePreq/{id1}/{id2}/{path}", method=RequestMethod.GET)
	public String deletePrequel(@PathVariable Long id1, @PathVariable Long id2, @PathVariable int path ) {
		Videogame vg = videogameRepository.findOne(id1);
		Videogame preq = videogameRepository.findOne(id2);

		vg.getPrequel().remove(preq);

		videogameRepository.save(vg);

		if (path == 1)
		{
			return "redirect:/newPrequel/{id1}";
		}else
		{
			return "redirect:/editPrequel/{id1}";
		}

	}

	@RequestMapping(value="/addPreq/{id1}/{id2}/{path}", method=RequestMethod.GET)
	public String addPrequel(@PathVariable Long id1, @PathVariable Long id2, @PathVariable int path) {
		Videogame vg = videogameRepository.findOne(id1);
		Videogame preq = videogameRepository.findOne(id2);

		List<Videogame> preq_already_inserted = vg.getPrequel();

		if (!(preq_already_inserted.isEmpty())) {
			if (path == 1)
				{
					return "redirect:/newPrequel/{id1}";
			}else
				{
					return "redirect:/editPrequel/{id1}";
				}
		}

		vg.getPrequel().add(preq);

		videogameRepository.save(vg);

		if (path == 1)
		{
			return "redirect:/newPrequel/{id1}";
		}else
		{
			return "redirect:/editPrequel/{id1}";
		}

	}

	@RequestMapping(value="/buyVideogame/{id1}/{id2}")
	public String buyVideogame(@PathVariable Long id1,@PathVariable Long id2, Model model) {
		model.addAttribute("videogame", videogameRepository.findOne(id1));
		model.addAttribute("user", userRepository.findOne(id2));
		return "buyVideogame";
	}

	@RequestMapping(value="/buyVideogame/{id1}/{id2}", method=RequestMethod.POST)
	public String buyVg(@PathVariable Long id1, @PathVariable Long id2, @RequestParam String n_copies, @RequestParam String date, Model model){
		try
		{
			Videogame vg = videogameRepository.findOne(id1);
			User user = userRepository.findOne(id2);
			

			int copies = Integer.parseInt(n_copies);			
			float costo =  Float.parseFloat(vg.getPrice());

			float tot = copies * costo;

			String total = Float.toString(tot);
			Buy b1 = new Buy(vg,user,date,n_copies,total);
			buyRepository.save(b1);
			
			vg.getUsersBought().add(b1);
			user.getVideogamesBought().add(b1);

			videogameRepository.save(vg);
			userRepository.save(user);

			return "redirect:/{id2}/videogames";
		}catch(Exception e)
		{
			model.addAttribute("type",1);
			model.addAttribute("errorString","Please insert a number inside the 'N° Copies'  field");
			model.addAttribute("idVideogame",id1);
			model.addAttribute("idUser",id2);
			return "errors";

		}	
	}

	@RequestMapping(value="/rentVideogame/{id1}/{id2}")
	public String rentVideogame(@PathVariable Long id1,@PathVariable Long id2, Model model) {
		model.addAttribute("videogame", videogameRepository.findOne(id1));
		model.addAttribute("user", deluxeUserRepository.findOne(id2));
		
		return "rentVideogame";
	}

	@RequestMapping(value="/rentVideogame/{id1}/{id2}", method=RequestMethod.POST)
	public String rentVg(@PathVariable Long id1, @PathVariable Long id2, @RequestParam String n_copies, @RequestParam String date,@RequestParam String endDate, Model model) {
		try
		{
			Videogame vg = videogameRepository.findOne(id1);
			DeluxeUser user = deluxeUserRepository.findOne(id2);

			int copies = Integer.parseInt(n_copies);			
			float costo =  Float.parseFloat(vg.getPrice());
			float tot = copies * costo;
			tot = tot - (tot * 30 / 100);
			double result = (double) Math.round(tot * 100.0) / 100.0;
			String total = Double.toString(result);

			Rent b1 = new Rent(vg,user,date,endDate,n_copies,total);
			rentRepository.save(b1);

			vg.getUsersRented().add(b1);
			user.getVideogamesRented().add(b1);

			videogameRepository.save(vg);
			deluxeUserRepository.save(user);

			return "redirect:/{id2}/videogames";
		}
		catch(Exception e)
		{
			model.addAttribute("type",2);
			model.addAttribute("errorString","Please insert a number inside the 'N° Copies'  field");
			model.addAttribute("idVideogame",id1);
			model.addAttribute("idUser",id2);
			return "errors";

		}	
	}
}
