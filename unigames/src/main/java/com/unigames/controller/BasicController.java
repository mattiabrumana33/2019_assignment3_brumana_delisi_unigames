package com.unigames.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BasicController {
  @RequestMapping("/index")
  public String home() {
    return "index";
  }

  @RequestMapping("/indexAdmin")
  public String homeAdmin() {
    return "indexAdmin";
  }
}
