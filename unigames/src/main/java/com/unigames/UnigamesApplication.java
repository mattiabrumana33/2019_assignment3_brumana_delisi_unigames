package com.unigames;

import com.unigames.model.*;
import com.unigames.repository.*;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class UnigamesApplication implements CommandLineRunner{

	@Autowired
	VideogameRepository videogameRepository;

	@Autowired
	PlatformRepository platformRepository;

	@Autowired
	GenreRepository genreRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	DeluxeUserRepository deluxeUserRepository;

	@Autowired
	BuyRepository buyRepository;

	@Autowired
	RentRepository rentRepository;

	public static void main(String[] args) {
		SpringApplication.run(UnigamesApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Platform p1 = new Platform("Playstation 4");
		Platform p2 = new Platform("xBox One");
		Platform p3 = new Platform("Playstation 3");
		Platform p4 = new Platform("Google Stadia");
		Platform p5 = new Platform("xBox 360");
		platformRepository.save(p1);
		platformRepository.save(p2);
		platformRepository.save(p3);
		platformRepository.save(p4);
		platformRepository.save(p5);

		Genre g1 = new Genre("Third Person Shooter");
		Genre g2 = new Genre("Adventure");
		Genre g3 = new Genre("Survival Horror");
		Genre g4 = new Genre("Stealth");
		Genre g5 = new Genre("Dynamic Adventure");
		genreRepository.save(g1);
		genreRepository.save(g2);
		genreRepository.save(g3);
		genreRepository.save(g4);
		genreRepository.save(g5);
		
		Videogame v1 = new Videogame("Days Gone", "2019", "Bend Studios", "100", "74.99", "yes", "no", 
											Arrays.asList(p1), Arrays.asList(g2,g3));
		Videogame v2 = new Videogame("Fortnite", "2017", "Epic Games", "50", "20.99", "yes", "yes",
											Arrays.asList(p1,p2,p4), Arrays.asList(g1));
		Videogame v3 = new Videogame("Hitman", "2016", "IO Interactive", "60", "34.00", "yes", "no",
											Arrays.asList(p1, p2), Arrays.asList(g4,g1));		
		Videogame v4 = new Videogame("Hitman 2", "2018", "IO Interactive", "10", "64.49", "yes", "no",
											Arrays.asList(p1,p4,p2), Arrays.asList(g4));
		Videogame v5 = new Videogame("Assassin's Creed", "2007", "Ubisoft Montreal", "45", "15.99", "no", "no",
											Arrays.asList(p3,p5), Arrays.asList(g5));
		Videogame v6 = new Videogame("Assassin's Creed II", "2009", "Ubisoft Montreal", "35", "17.99", "no", "no",
											Arrays.asList(p3,p5), Arrays.asList(g4,g5));
		Videogame v7 = new Videogame("Dishonored", "2012", "Arkane Studios", "20", "35.99", "no", "yes",
											Arrays.asList(p1,p2,p3,p5), Arrays.asList(g4));											    
		videogameRepository.save(v1);
		videogameRepository.save(v2);
		videogameRepository.save(v3);
		videogameRepository.save(v4);
		videogameRepository.save(v5);
		videogameRepository.save(v6);
		videogameRepository.save(v7);

		User u1 = new BasicUser("Mattia", "Brumana", "basic@gmail.com", "BRMMTT96P23W54G", "basicpsw");
		DeluxeUser u2 = new DeluxeUser("Leonardo Antonio", "De Lisi", "deluxe@gmail.com", "DLSLNN96G12A345G", "deluxepsw");
		userRepository.save(u1);
		userRepository.save(u2);

		Buy b1 = new Buy(v1, u1, "6/1/2020", "2", "149.98");
		Buy b2 = new Buy(v5, u2, "9/1/2020", "1", "15.99");
		buyRepository.save(b1);
		buyRepository.save(b2);

		Rent r1 = new Rent(v2, u2, "8/1/2020", "8/3/2020", "1", "14.69");
		rentRepository.save(r1);

		p1.setVideogames(Arrays.asList(v1,v2,v3,v4,v7));
		p2.setVideogames(Arrays.asList(v2,v3,v4,v7));
		p3.setVideogames(Arrays.asList(v5,v6,v7));
		p4.setVideogames(Arrays.asList(v2,v4));
		p5.setVideogames(Arrays.asList(v5,v6,v7));
		platformRepository.save(p1);
		platformRepository.save(p2);
		platformRepository.save(p3);
		platformRepository.save(p4);
		platformRepository.save(p5);

		g1.setVideogames(Arrays.asList(v2,v3));
		g2.setVideogames(Arrays.asList(v1));
		g3.setVideogames(Arrays.asList(v1));
		g4.setVideogames(Arrays.asList(v3,v4,v6,v7));
		g5.setVideogames(Arrays.asList(v5,v6));
		genreRepository.save(g1);
		genreRepository.save(g2);
		genreRepository.save(g3);
		genreRepository.save(g4);
		genreRepository.save(g5);

		v4.setPrequel(Arrays.asList(v3));
		v6.setPrequel(Arrays.asList(v5));
		videogameRepository.save(v4);
		videogameRepository.save(v6);

		v1.setUsersBought(Arrays.asList(b1));
		u1.setVideogamesBought(Arrays.asList(b1));
		v5.setUsersBought(Arrays.asList(b2));
		u2.setVideogamesBought(Arrays.asList(b2));
		videogameRepository.save(v1);
		videogameRepository.save(v5);
		userRepository.save(u1);
		userRepository.save(u2);

		v2.setUsersRented(Arrays.asList(r1));
		u2.setVideogamesRented(Arrays.asList(r1));
		videogameRepository.save(v2);
		deluxeUserRepository.save(u2);
	}
}