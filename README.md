# REQUIRED INSTALLATIONS TO RUN THE PROJECT
+ JAVA openjdk version 13.0.1 2019-10-15
+ APACHE MAVEN version 3.6.3
+ SPRING CLI v2.2.1.RELEASE

# HOW TO RUN THE PROJECT
+ Move to *"unigames"* directory
+ run `mvn clean install -Dmaven.test.skip=true`
+ run `mvn spring-boot:run -Dmaven.test.skip=true`
+ Move to `localhost:8181/index`

# LOGIN CREDENTIALS
+ Login as Basic User with Email: **basic@gmail.com** and Password: **basicpsw**
+ Login as Deluxe User with Email: **deluxe@gmail.com** and Password: **deluxepsw**
+ Login as Admin with Email: admin and Password: admin